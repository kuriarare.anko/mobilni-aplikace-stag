import { Component, OnInit } from '@angular/core';
import { DbApiService } from 'src/app/services/db-api.service';
import { Uzivatel } from '../providers/uzivatel';

@Component({
    selector: 'app-planovac-zapsane',
    templateUrl: './planovac-zapsane.page.html',
    styleUrls: ['./planovac-zapsane.page.scss'],
})
export class PlanovacZapsanePage implements OnInit {
    semestr = 'ZS';
    rocnik = 1;
    predmety;
    predmetyZobraz;
    schovaneUpozorneni = true;


    constructor(private dbApiService: DbApiService, public uzivatel: Uzivatel) { }

    /**
     * Na�te ulo�en� pl�n z datab�ze a nastav� jeho zobrazen�. Pokud nen� ulo�en ��dn� pl�n, zobraz� se upozorn�n�.
     * */
    ngOnInit() {
        this.predmety = new Map();
        this.predmetyZobraz = new Map();

        this.dbApiService.nacti(this.uzivatel.osCislo).then(value => {
            console.log(value);
            if (value != undefined) {
                this.predmety = this.zJsonu(value.data);
                console.log(this.predmety);
                for (let entry of Array.from(this.predmety.entries())) {
                    this.predmetyZobraz.set(entry[0], new DataSemestru({ data: entry[1], semestr: this.semestr, rocnik: this.rocnik }));
                    this.posunSemestr();
                }
            }
            else {
                this.schovaneUpozorneni = false;
            }
        });
    }

    posunSemestr() {
        if (this.semestr == "ZS") {
            this.semestr = "LS";
        } else {
            this.semestr = "ZS";
            this.rocnik++;
        }
    }

    /**
    * P�evede JSON p�ed�van� v prom�nn� json do mapy.
    * @param json - JSON pro p�eveden�
    * @return Map - p�eveden� JSON
    */
    zJsonu(json) {
        return new Map(JSON.parse(json));
    }
    vratZaznamy() {
        return Array.from(this.predmetyZobraz.entries());
    }
}

/**
 * T��da reprezentuj�c� data semestru.
 * */
export class DataSemestru {
    data;
    semestr;
    rocnik;

    constructor(data: DataSemestru) {
        Object.assign(this, data);
    }
}
