import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StagApiService } from 'src/app/services/stag-api.service';

@Component({
    selector: 'app-student',
    templateUrl: './student.page.html',
    styleUrls: ['./student.page.scss'],
})
export class StudentPage implements OnInit {
    student: StudentInfo;
    zchovatInfo = false;
    zchovatPredmety = true;
    predmetyStudent;

    constructor(private route: ActivatedRoute, private router: Router, private stagApiService: StagApiService) { }

    /**
    * Pomoc� parametr� p�edan�ch routerem nastav� prom�nnou student a vyhled� p�edm�ty pro zobrazovan�ho studenta.
    * */
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                console.log(this.router.getCurrentNavigation().extras.state.json);
                this.student = new StudentInfo(this.router.getCurrentNavigation().extras.state.json);
            }
        });

        this.stagApiService.getPredmetyByStudent(this.student.osCislo).then(value => {
            console.log("nastavene predmety - " + value);
            this.nastavPredmety(value);
        });;
    }

    /**
     * P�ep�na zobrazovan� informace.
     * @param id - 1 - informace o studentovi, 2 - p�edm�ty studenta
     */
    prepniZobrazeni(id) {
        switch (id) {
            case 1:
                this.zchovatInfo = false;
                this.zchovatPredmety = true;
                break;
            case 2:
                this.zchovatInfo = true;
                this.zchovatPredmety = false;
                break;
            default:
                break;
        }
    }

    nastavPredmety(jsonPredmet) {
        this.predmetyStudent = new Array();
        let json = JSON.parse(jsonPredmet);
        let i = 0;
        for (const item of json.predmetStudenta) {
            this.predmetyStudent[i] = new PredmetStudent({ zkratka: item.katedra + "/" + item.zkratka, nazev: item.nazev });
            i++;
        }
    }
}

/**
 * T��da reprezentuj�c� data p�edm�tu.
 * */
export class PredmetStudent {
    zkratka;
    nazev;
    constructor(data: PredmetStudent) {
        Object.assign(this, data);
    }
}

/**
 * T��da reprezentuj�c� data studenta.
 * */
export class StudentInfo {
    osCislo;
    jmeno;
    prijmeni;
    tituly = "";
    zobrazTituly = true;
    nazevSp;
    studijniProgram;
    studijniObor;
    fakultaSp;
    kodSp;
    formaSp;
    rocnik;
    oborKomb;
    oborIdnos;
    email;

    constructor(jsonStudent) {
        this.nastav(jsonStudent);
        console.log(jsonStudent);
    }

    nastav(infoJson) {
        let json = JSON.parse(infoJson);

        this.osCislo = json.osCislo;
        this.jmeno = json.jmeno ? json.jmeno : "-";
        this.prijmeni = json.prijmeni ? json.prijmeni : "-";
        this.nazevSp = json.nazevSp ? json.nazevSp : "-";

        this.studijniProgram = json.kodSp + " - " + json.nazevSp;
        this.studijniObor = json.oborKomb;
        this.email = json.email ? json.email : "-"
        this.rocnik = json.rocnik ? json.rocnik : "-";

        this.tituly = json.titulPred;
        if (json.titulZa != null) {
            this.tituly += " " + json.titulZa;
        }
        if (json.tituly != null) {
            this.zobrazTituly = false;
        }
    }
}