import { Injectable } from '@angular/core';

/**
 * T��da reprezentuj�c� data u�ivatele.
 * */
@Injectable()
export class Uzivatel {
    osCislo;
    role;
    fakulta;

    jmeno;
    prijmeni;
    tituly;
    zobrazTituly = true;
    nazevSp;
    fakultaSp;
    kodSp;
    rocnik;
    email;
    oborIdnos;

    prihlasen = false;


    constructor() { }
}