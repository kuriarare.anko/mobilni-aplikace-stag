import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MistnostPage } from './mistnost.page';

describe('MistnostPage', () => {
  let component: MistnostPage;
  let fixture: ComponentFixture<MistnostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MistnostPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MistnostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
