import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Mistnost } from '../hledani/hledani.page';

@Component({
  selector: 'app-mistnost',
  templateUrl: './mistnost.page.html',
  styleUrls: ['./mistnost.page.scss'],
})
export class MistnostPage implements OnInit {

    mistnost: Mistnost;
    idMistnost;

    constructor(private route: ActivatedRoute, private router: Router) { }

    /**
     * Pomoc� parametr� p�edan�ch routerem nastav� prom�nnou mistnost a idMistnost.
     * */
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.mistnost = this.router.getCurrentNavigation().extras.state.mistnost;
                this.idMistnost = this.mistnost.zkrBudovy +"/"+ this.mistnost.cisloMistnosti;
            }
        });
    }

}
