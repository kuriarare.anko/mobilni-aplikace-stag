import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VyhledatPage } from './vyhledat.page';

describe('VyhledatPage', () => {
  let component: VyhledatPage;
  let fixture: ComponentFixture<VyhledatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VyhledatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VyhledatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
