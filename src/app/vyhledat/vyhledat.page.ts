import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Uzivatel } from '../providers/uzivatel';

@Component({
    selector: 'app-vyhledat',
    templateUrl: './vyhledat.page.html',
    styleUrls: ['./vyhledat.page.scss'],
})
export class VyhledatPage implements OnInit {

    neprihlasen: boolean;

    constructor(private router: Router, private uzivatel: Uzivatel) {
        this.neprihlasen = !(this.uzivatel.prihlasen);
    }

    ngOnInit() {
    }

    /**
     * Pomoc� routeru p�ejde na str�nku hled�n� a z�rove� j� p�ed� informaci o zm��knut�m tla��tku.
     * @param id - 1 - u�itel�, 2 - studenti, 3 - p�edm�ty, 4 - m�stnosti
     */
    klikZobrazeni(id) {
        let navigationExtras: NavigationExtras = {
            state: {
                cislo: id,
            }
        };
        this.router.navigate(['hledani'], navigationExtras);
    }

}
