import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HledaniPage } from './hledani.page';

describe('HledaniPage', () => {
  let component: HledaniPage;
  let fixture: ComponentFixture<HledaniPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HledaniPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HledaniPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
