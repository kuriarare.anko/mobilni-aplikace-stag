import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { StagApiService } from 'src/app/services/stag-api.service';

@Component({
    selector: 'app-hledani',
    templateUrl: './hledani.page.html',
    styleUrls: ['./hledani.page.scss'],
})
export class HledaniPage implements OnInit {

    hledano;
    hlavicka;
    text1;
    text2;
    text3;
    parametr1 = "";
    parametr2 = "";
    parametr3 = "";

    schovane = false;
    textChyba = "Zadejte alespoň jeden z parametrů.";
    zobrazChyba = true;
    upozorneni = true;

    ucitele: Ucitel[];
    predmety: Predmet[];
    studenti: Student[];
    mistnosti: Mistnost[];

    constructor(private route: ActivatedRoute, private router: Router, private stagApiService: StagApiService) {
    }

    /**
     * Pomocí routeru zjistí předávaný parametr a použije ho pro nastavení zobrazení stránky
     * */
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.zobrazPodleParametru(this.router.getCurrentNavigation().extras.state.cislo);
            }
        });
    }

    /**
     * Nastaví zobrazení stránky na základě předávaného parametru param.
     * @idTlacitka idTlacitka - cislo pro nastavení stránky [1 - učitelé, 2 - studenti, 3 - předměty, 4 - místnosti]
     */
    zobrazPodleParametru(idTlacitka) {
        switch (idTlacitka) {
            case 1: {
                this.nastavUcitele();
                break;
            }
            case 2: {
                this.nastavStudenti();
                break;
            }
            case 3: {
                this.nastavPredmnety();
                break;
            }
            case 4: {
                this.nastavMistnosti();
                break;
            }
            default: {
                break;
            }
        }
        this.hledano = idTlacitka;
    }

    nastavUcitele() {
        this.schovane = true;
        this.text1 = "Jméno";
        this.text2 = "Příjmení";
        this.hlavicka = "učitelů";
    }

    nastavStudenti() {
        this.schovane = true;
        this.text1 = "Jméno";
        this.text2 = "Příjmení";
        this.hlavicka = "studentů";
    }

    nastavPredmnety() {
        this.schovane = false;
        this.text1 = "Pracoviště";
        this.text2 = "Zkratka";
        this.text3 = "Název";
        this.hlavicka = "předmětů";
    }

    nastavMistnosti() {
        this.schovane = true;
        this.text1 = "Zkratka budovy";
        this.text2 = "Číslo místnosti";
        this.hlavicka = "místností";
    }

    /**
     * Metoda odstraní všech upozornení zobrazená uživateli a zbaví parametry prázdných míst. 
     * Poté podle uloženého parametru hledano zavolá metodu pro vyhledávání.
     * Pokud není vyplňen ani jeden parametr, zobrazí se upozornění.
     * */
    klikHledat() {
        this.zobrazChyba = true;
        this.upozorneni = true;
        this.parametr1 = this.parametr1.replace(/\s/g, "");
        this.parametr2 = this.parametr2.replace(/\s/g, "");
        this.parametr3 = this.parametr3.replace(/\s/g, "");
        switch (this.hledano) {
            case 1: {
                if (this.parametr1 == "" && this.parametr2 == "") {
                    this.zobrazChyba = false;
                } else {
                    this.vyhledejUcitele(this.parametr1, this.parametr2);
                }
                break;
            }
            case 2: {
                if (this.parametr1 == "" && this.parametr2 == "") {
                    this.zobrazChyba = false;
                } else {
                    this.vyhledejStudenty(this.parametr1, this.parametr2);
                }
                break;
            }
            case 3: {
                if (this.parametr1 == "" && this.parametr2 == "" && this.parametr3 == "") {
                    this.zobrazChyba = false;
                } else {
                    this.vyhledejPredmety(this.parametr1, this.parametr2, this.parametr3);
                }
                break;
            }
            case 4: {
                if (this.parametr1 == "" && this.parametr2 == "") {
                    this.zobrazChyba = false;
                } else {
                    this.vyhledejMistnosti(this.parametr1, this.parametr2);
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * Podle zadaných parametrů vyhledá učitele a uloží je do pole pomocí třídy Ucitel.
     * @param jmeno - jméno učitele
     * @param prijmeni - příjmení učitele
     */
    vyhledejUcitele(jmeno: string, prijmeni: string) {
        this.stagApiService.najdiUcitelePodleJmena(jmeno, prijmeni).then(value => {
            this.ucitele = new Array<Ucitel>();
            let json = JSON.parse(value);
            if (json.ucitel.length == 0) {
                this.upozorneni = false;
            } else
            for (const ucitel of json.ucitel) {
                this.ucitele.push(new Ucitel(ucitel.titulPred, ucitel.jmeno, ucitel.prijmeni, ucitel.ucitIdno));
            }
        });
    }

    /**
     *Podle zadaných parametrů vyhledá studenty a uloží je do pole pomocí třídy Student.
     * @param jmeno - jméno studenta
     * @param prijmeni - příjmení studneta
     */
    vyhledejStudenty(jmeno: string, prijmeni: string) {
        this.stagApiService.najdiStudentyPodleJmena(jmeno, prijmeni).then(value => {
            this.studenti = new Array<Student>();
            let json = JSON.parse(value);
            if (json.student.length == 0) {
                this.upozorneni = false;
            } else
                for (const student of json.student) {
                    this.studenti.push(new Student(student.osCislo, student.jmeno, student.prijmeni));
                }
        });
    }

    /**
     * Podle zadaných parametrů vyhledá učitele a uloží je do pole pomocí třídy Predmet.
     * @param nazev - název předmětu
     * @param pracoviste - zkratka pracoviště předmětu
     * @param zkratka - zkratka předmětu
     */
    vyhledejPredmety(nazev: string, pracoviste: string, zkratka: string) {
        this.stagApiService.najdiPredmety(nazev, pracoviste, zkratka).then(value => {
            this.predmety = new Array<Predmet>();
            let json = JSON.parse(value);

            if (json.predmetKatedry.length == 0) {
                this.upozorneni = false;
            }  else
                for (const predmet of json.predmetKatedry) {
                    this.predmety.push(new Predmet(predmet.katedra, predmet.zkratka, predmet.nazev));
                }
        });
    }

    /**
     * Podle zadaných parametrů vyhledá učitele a uloží je do pole pomocí třídy Mistnost.
     * @param zkrBudovy - zkratka budovy, ve které se místnost nachází
     * @param cisloMistnosti - číslo místnosti
     */
    vyhledejMistnosti(zkrBudovy: string, cisloMistnosti: string) {
        this.stagApiService.getMistnostiInfo(zkrBudovy, cisloMistnosti).then(value => {
            this.mistnosti = new Array<Mistnost>();
            let json = JSON.parse(value);
            if (json.mistnostInfo.length == 0) {
                this.upozorneni = false;
            }else
                for (const mistnost of json.mistnostInfo) {
                    this.mistnosti.push(new Mistnost(mistnost));
                }
        });
    }

    /**
     * Vyhledá informace o předmětu a pomocí routeru je předá stránce predmet a přejde na ní.
     * @param katedra - zkratka katedry předmětu
     * @param zkratka - zkratka předmětu
     */
    klikPredmet(katedra, zkratka) {
        this.stagApiService.getPredmetInfo(katedra, zkratka).then(value => {
            let navigationExtras: NavigationExtras = {
                state: {
                    json: value
                }
            };
            this.router.navigate(['predmet'], navigationExtras);
        });

    }

    /**
     * Vyhledá informace o studentovi a pomocí routeru je předá stránce student a přejde na ní.
     * @param osCislo - osobní číslo studenta
     */
    klikStudent(osCislo) {
        this.stagApiService.getStudentInfo(osCislo).then(value => {
            let navigationExtras: NavigationExtras = {
                state: {
                    json: value
                }
            };
            this.router.navigate(['student'], navigationExtras);
        });

    }

    /**
     * Vyhledá informace o učiteli a pomocí routeru je předá stránce ucitel a přejde na ní.
     * @param ucitIdno - identifikační číslo učitele
     */
    klikUcitel(ucitIdno) {
        this.stagApiService.getUcitelInfo(ucitIdno).then(value => {
            let navigationExtras: NavigationExtras = {
                state: {
                    json: value
                }
            };
            this.router.navigate(['ucitel'], navigationExtras);
        });

    }

    /**
     * Pomocí routeru předá stránce mistnost a přejde na ní.
     * @param mistnost [Mistnost] - vybrané místnost
     */
    klikMistnost(mistnost) {
        let navigationExtras: NavigationExtras = {
            state: {
                mistnost: mistnost
            }
        };
        this.router.navigate(['mistnost'], navigationExtras);


    }

}

/**
 * Třída reprezentující data studenta.
 * */
export class Student {
    osCislo;
    jmeno;
    prijmeni;

    constructor(osCislo: string, jmeno: string, prijmeni: string) {
        this.osCislo = osCislo;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }
}

/**
 * Třída reprezentující data předmětu.
 * */
export class Predmet {
    katedra;
    zkratka;
    nazev;

    constructor(katedra: string, zkratka: string, nazev: string) {
        this.katedra = katedra;
        this.zkratka = zkratka;
        this.nazev = nazev;
    }
}

/**
 * Třída reprezentující data učitele.
 * */
export class Ucitel {
    titulPred;
    jmeno;
    prijmeni;
    ucitIdno;

    constructor(titulPred: string, jmeno: string, prijmeni: string, ucitIdno: string) {
        this.titulPred = titulPred;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.ucitIdno = ucitIdno;
    }
}

/**
 * Třída reprezentující data místnosti.
 * */
export class Mistnost {
    zkrBudovy;
    cisloMistnosti;
    podlazi;
    katedra;
    pracoviste;
    typ;
    kapacita;
    spolecnyFond;
    obec;
    ulice;
    cisloPopisne;

    constructor(jsonMistnost) {
        if (jsonMistnost == ">/=-") {
            this.zkrBudovy = "Nebyl nalezen žádný výsledek.";
        }
        this.nastav(jsonMistnost);
    }

    nastav(infoJson) {
        this.zkrBudovy = infoJson.zkrBudovy;
        this.cisloMistnosti = infoJson.cisloMistnosti;
        this.podlazi = infoJson.podlazi;
        this.katedra = infoJson.katedra;
        this.pracoviste = infoJson.pracoviste;
        this.typ = infoJson.typ;
        this.kapacita = infoJson.kapacita;
        this.spolecnyFond = infoJson.spolecnyFond;
        this.obec = infoJson.obec;
        this.ulice = infoJson.ulice;
        this.cisloPopisne = infoJson.cisloPopisne;
    }
}