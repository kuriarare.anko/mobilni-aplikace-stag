import { Component } from '@angular/core';
import { StagApiService } from 'src/app/services/stag-api.service';
import { Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    toast;
    constructor(private router: Router, private stagApiService: StagApiService, private platform: Platform, private network: Network, public toastController: ToastController) {
        this.nastavToast(false);
        let connectSubscription = this.network.onConnect().subscribe(() => {
            this.toast.dismiss();
        });
        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            this.nastavToast(true);
        });
    }

    /**
     * Zavolá metodu pro přihlášení. Pokud je přihlášení v pořátku a role přihlášeného uživatele je student,
     * nastaví providera uzivatel na základě vrácených informací  a přejde na stránku hlavni-strana.
     * */
    otevriPrihlasit() {
        this.platform.ready().then(() => {
            this.stagApiService.prihlaseni().then(success => {
                let rozsifrovana_data = JSON.parse(atob(success.json));
                if (rozsifrovana_data.stagUserInfo[0].role != "ST") {
                    alert("Přihlášený uživatel nemá roli student.");
                }
                else {
                    this.stagApiService.nastavUzivatele(rozsifrovana_data);
                    this.router.navigate(['hlavni-strana']);
                }
            }, (error) => {
                alert(error);
            })
        });
    }

    async nastavToast(zobrazit) {
        this.toast = await this.toastController.create({
            message: "Zkontrolujte připojení k internetu.",
            position: "top"
        });
        if (zobrazit)
            this.toast.present();
    }
}
