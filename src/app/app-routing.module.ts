import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: './home/home.module#HomePageModule' },
    { path: 'vyhledat', loadChildren: './vyhledat/vyhledat.module#VyhledatPageModule' },
    { path: 'hledani', loadChildren: './hledani/hledani.module#HledaniPageModule' },
    { path: 'hlavni-strana', loadChildren: './hlavni-strana/hlavni-strana.module#HlavniStranaPageModule' },
  { path: 'predmet', loadChildren: './predmet/predmet.module#PredmetPageModule' },
  { path: 'ucitel', loadChildren: './ucitel/ucitel.module#UcitelPageModule' },
  { path: 'student', loadChildren: './student/student.module#StudentPageModule' },
  { path: 'moje-znamky', loadChildren: './moje-znamky/moje-znamky.module#MojeZnamkyPageModule' },
  { path: 'zkousky', loadChildren: './zkousky/zkousky.module#ZkouskyPageModule' },
  { path: 'info-uzivatel', loadChildren: './info-uzivatel/info-uzivatel.module#InfoUzivatelPageModule' },
  { path: 'planovac', loadChildren: './planovac/planovac.module#PlanovacPageModule' },
  { path: 'pridat-predmet', loadChildren: './pridat-predmet/pridat-predmet.module#PridatPredmetPageModule' },
  { path: 'planovac-zapsane', loadChildren: './planovac-zapsane/planovac-zapsane.module#PlanovacZapsanePageModule' },
  { path: 'mistnost', loadChildren: './mistnost/mistnost.module#MistnostPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
