import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { StagApiService } from '../services/stag-api.service';

@Component({
  selector: 'app-pridat-predmet',
  templateUrl: './pridat-predmet.page.html',
  styleUrls: ['./pridat-predmet.page.scss'],
})
export class PridatPredmetPage implements OnInit {

    pracoviste="";
    zkratka="";
    nazev="";
    zobrazChyba = true;
    predmety: Predmet[];
    json = null;
    textChyba = "Zadejte alespo� jeden z parametr�.";

    constructor(public modalController: ModalController, private stagApiService: StagApiService) { }

    ngOnInit() { }

    /**
     * Zkontroluje zda je zad�n alespo� jeden z parametr�, kter� se vyu��vaj� k vyhled�v�n� a pokud je kontrola
     * spln�na zavol� metodu pro vyhled�v�n�. 
     * */
    klikHledat() {
        this.pracoviste = this.pracoviste.replace(/\s/g, "");
        this.zkratka = this.zkratka.replace(/\s/g, "");
        this.nazev = this.nazev.replace(/\s/g, "");
        if (this.pracoviste == "" && this.zkratka == "" && this.nazev == "") {
            this.zobrazChyba = false;
        } else {
            this.zobrazChyba = true;
            this.vyhledejPredmety(this.pracoviste, this.zkratka, this.nazev);
        }
    }

    /**
     * Pomoc� metody v stagApiService a zadan�ch parametr� vyhled� p�edm�ty, kter� n�sledn� vlo�� do pole predmety.
     * @param nazev - n�zev p�edm�tu
     * @param pracoviste - zkratka pracovi�t� p�edm�tu
     * @param zkratka - zkratka p�edm�tu
     */
    vyhledejPredmety(nazev: string, pracoviste: string, zkratka: string) {
        this.stagApiService.najdiPredmety(nazev, pracoviste, zkratka).then(value => {
            console.log(value);
            this.predmety = new Array<Predmet>();
            let json = JSON.parse(value);

            for (const predmet of json.predmetKatedry) {
                this.predmety.push(new Predmet(predmet.katedra, predmet.zkratka, predmet.nazev));
            }
        });
    }

    /**
     * Pomoc� metody v stagApiService a zadan�ch parametr� vyhled� podrobn� informace o p�edm�tu, kter� p�ed� str�nce 
     * jen� modal vyvolala a zav�e se.
     * @param katedra - zkratka katedry p�edm�tu
     * @param zkratka - zkratka p�edm�tu
     */
    klikPredmet(katedra, zkratka) {
        this.stagApiService.getPredmetInfo(katedra, zkratka).then(value => {
            this.json = value;
            this.zavri();
        });
        
    }

    zavri() {
        this.modalController.dismiss(this.json);
    }
 
}

/**
 * T��da reprezentuj�c� data p�edm�tu.
 * */
export class Predmet {
    katedra;
    zkratka;
    nazev;

    constructor(katedra: string, zkratka: string, nazev: string) {
        this.katedra = katedra;
        this.zkratka = zkratka;
        this.nazev = nazev;
    }
}

