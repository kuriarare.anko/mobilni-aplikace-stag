import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PridatPredmetPage } from './pridat-predmet.page';

describe('PridatPredmetPage', () => {
  let component: PridatPredmetPage;
  let fixture: ComponentFixture<PridatPredmetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PridatPredmetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PridatPredmetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
