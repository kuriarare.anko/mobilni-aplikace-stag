import { Component, OnInit, Input } from '@angular/core';
import { StagApiService } from 'src/app/services/stag-api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-rozvrh',
    templateUrl: './rozvrh.component.html',
    styleUrls: ['./rozvrh.component.scss'],
})

export class RozvrhComponent implements OnInit {
    @Input() id: string;
    @Input() druh: string;
    schovano = true;
    neniPredmet = false;
    btnRozvrh = "Zobrazit rozvrh";
    ikonkaRozkliknout = "arrow-dropdown-circle";

    vyskaDnePole = [0, 0, 0, 0, 0];
    htmlPole = new Array();
    rozvrhAkceMap;
    vyskaRadku = 60;

    pondeli;
    utery;
    streda;
    ctvrtek;
    patek;
    bezRozvrhu;

    htmlPo;
    htmlUt;
    htmlSt;
    htmlCt;
    htmlPa;
    htmlBezRozvrhu;

    constructor(private stagApiService: StagApiService, private sanitized: DomSanitizer) { }

    /**
     * Metoda podle vkl�dan� prom�nn� (druh) zavol� p��slu�nou metodu pro z�sk�n� rozvrhu.
     * 1 - student, 2 - ucitel, 3 - predmet, 4 - mistonst
     * */
    ngOnInit() {
        switch (this.druh) {
            case '1': {
                this.nastavRozvrhStudent();
                break;
            }
            case '2': {
                this.nastavRozvrhUcitel();
                break;
            }
            case '3': {
                this.nastavRozvrhPredmet();
            }
            case '4': {
                this.nastavRozvrhMistnost();
            }
        }
    }

    /**
     * Zobrazuje a schov�v� rozvrh.
     * */
    zobrazRozvrh() {
        this.schovano = !(this.schovano);

        if (this.schovano) {
            this.btnRozvrh = "Zobrazit rozvrh";
            this.ikonkaRozkliknout = "arrow-dropdown-circle";
        } else {
            this.btnRozvrh = "Skryt rozvrh";
            this.ikonkaRozkliknout = "arrow-dropup-circle";
        }
    }

    nastavRozvrhStudent() {
        this.stagApiService.getRozvrhByStudent(this.id).then(value => {
            this.nastav(value);
        });
    }

    nastavRozvrhUcitel() {
        this.stagApiService.getRozvrhByUcitel(this.id).then(value => {
            this.nastav(value);
        });
    }

    nastavRozvrhPredmet() {
        let katedraZkratka = this.id.split("/");
        console.log(this.id);
        this.stagApiService.getRozvrhByPredmet(katedraZkratka[0], katedraZkratka[1]).then(value => {
            this.nastav(value);
        });
    }

    nastavRozvrhMistnost() {
        let idMistnost = this.id.split("/");
        this.stagApiService.getRozvrhByMistnost(idMistnost[0], idMistnost[1]).then(value => {
            this.nastav(value);
        });
    }

    /**
     * Parametr infoJson vyfiltruje podle toho zda m� nastaven� id a ulo�� jej ve form�tu Map ve t��d� RozvrhovaAkce.
     * Pot� zavol� metodu pro nastaven� jednotliv�ch dn� (nastavDen), nastav� v��ku jednotliv�ch dn� v HTML souboru 
     * a obejde bezpecnostni politiku HTML pro zobrazen� rozvrhu.
     * @param infoJson - JSON rozvrhu vr�cen� serverem IS/STAG
     */
    nastav(infoJson) {
        this.rozvrhAkceMap = new Map();
        let json = JSON.parse(infoJson).rozvrhovaAkce.filter(function (item) {
            return item.roakIdno != "-";
        });

        for (const item of json) {
            let ra = new RozvrhovaAkce(item);
            if (!this.rozvrhAkceMap.has(ra.denZkr)) {
                this.rozvrhAkceMap.set(ra.denZkr, new Array(ra));
            }
            else {
                this.rozvrhAkceMap.get(ra.denZkr).push(ra);
            }
        }

        this.htmlPole = ["", "", "", "", ""];

        this.nastavDenVTydnu("Po", this.pondeli, 0);
        this.nastavDenVTydnu("�t", this.utery, 1);
        this.nastavDenVTydnu("St", this.streda, 2);
        this.nastavDenVTydnu("�t", this.ctvrtek, 3);
        this.nastavDenVTydnu("P�", this.patek, 4);

        this.bezRozvrhu = new Array();
        if (this.rozvrhAkceMap.get("-") != undefined) {
            this.bezRozvrhu = this.rozvrhAkceMap.get("-").filter(function (item) {
                return item.roakIdno != "-" && item.platnost == "Plati";
            });
        } else {
            this.neniPredmet = true;
        }

        document.body.style.setProperty('--vyskaPodneli', this.vyskaDnePole[0] + "px");
        document.body.style.setProperty('--vyskaUtery', this.vyskaDnePole[1] + "px");
        document.body.style.setProperty('--vyskaStreda', this.vyskaDnePole[2] + "px");
        document.body.style.setProperty('--vyskaCtvrtek', this.vyskaDnePole[3] + "px");
        document.body.style.setProperty('--vyskaPatek', this.vyskaDnePole[4] + "px");

        this.htmlPo = this.sanitized.bypassSecurityTrustHtml(this.htmlPole[0]);
        this.htmlUt = this.sanitized.bypassSecurityTrustHtml(this.htmlPole[1]);
        this.htmlSt = this.sanitized.bypassSecurityTrustHtml(this.htmlPole[2]);
        this.htmlCt = this.sanitized.bypassSecurityTrustHtml(this.htmlPole[3]);
        this.htmlPa = this.sanitized.bypassSecurityTrustHtml(this.htmlPole[4]);
    }

    /**
     * Metoda zavol� jin� metody pro nastaven� dne v rozvrhu.
     * @param zkratkaDne - zkratka dne: Po, �t, St, �t, P�
     * @param den - pole do kter�ho se ulo�� den
     * @param cisloVPoli - identifika�n� ��slo dne
     */
    nastavDenVTydnu(zkratkaDne, den, cisloVPoli) {
        if (this.rozvrhAkceMap.get(zkratkaDne) != undefined) {
            den = this.rozvrhAkceMap.get(zkratkaDne);
            if (den != undefined) {
                this.vypocitejVyskuRadku(den);
                this.vytvorDenVTydnu(den, cisloVPoli);
            } else {
                this.vyskaDnePole[cisloVPoli] = this.vyskaRadku;
            }
        } else {
            this.vyskaDnePole[cisloVPoli] = this.vyskaRadku;
        }
    }

    /**
     * Vypo��t� v��ku ��dku dne na z�klad� po�tu p�ekr�vaj�c�ch se p�edm�t�.
     * @param den - den, pro kter� se po��t� v��ka
     */
    vypocitejVyskuRadku(den) {
        for (let i = 0; i < den.length; i++) {
            for (let y = i; y < den.length; y++) {
                if (den[y] != den[i]) {
                    if ((den[i].hodinaSkutOd < den[y].hodinaSkutOd && den[y].hodinaSkutOd < den[i].hodinaSkutDo ||
                        den[y].hodinaSkutOd < den[i].hodinaSkutOd && den[i].hodinaSkutOd < den[y].hodinaSkutDo)) {

                        if (den[y].cisloRadku > den[i].cisloRadku) {
                            den[i].cisloRadku = den[y].cisloRadku + 1;
                        } else {
                            den[y].cisloRadku = den[i].cisloRadku;
                            den[i].cisloRadku++;
                        }
                    }
                }
            }
        }
    }

    /**
     * Nastav� HTML k�d dne.
     * @param denVTydnuPole - den, pro kter� se HTLM nastavuje
     * @param cisloDneVTydnu - ��slo dne v t�dnu [0-4]
     */
    vytvorDenVTydnu(denVTydnuPole, cisloDneVTydnu) {
        let hodina = 100;
        let minuta = hodina / 60;
        let zacatekVyuc = 700;

        for (const rozvrhovaAkce of denVTydnuPole) {
            let zacatek = ((Number(rozvrhovaAkce.hodinaSkutOdHodinyMin[0]) * hodina) + (Number(rozvrhovaAkce.hodinaSkutOdHodinyMin[1]) * minuta) - zacatekVyuc); //zatim jenom H bez Min
            let konec = ((Number(rozvrhovaAkce.hodinaSkutDoHodinyMin[0]) * hodina) + (Number(rozvrhovaAkce.hodinaSkutDoHodinyMin[1]) * minuta) - zacatekVyuc);
            let trvani = konec - zacatek;
            this.htmlPole[cisloDneVTydnu] += '<ion-col class="blokVRadku" style="width:' + trvani + 'px;left:' + zacatek + 'px;top:' + rozvrhovaAkce.cisloRadku * this.vyskaRadku + 'px;display:block;background-color:' + rozvrhovaAkce.barva + ';border:#d33939 1px solid;height:' + this.vyskaRadku + 'px;vertical-align: top;position:absolute;font-size: 9px;padding:0;">' + '<ion-row style="height: 17px;"><ion-col justify-content-start>' + rozvrhovaAkce.hodinaSkutOd + '</ion-col><ion-col style="text-align: end;">' + rozvrhovaAkce.hodinaSkutDo + '</ion-col></ion-row><ion-row justify-content-center style="font-size: 12px;"><b>' + rozvrhovaAkce.katedra + '/' + rozvrhovaAkce.predmet + '</b></ion-row><ion-row justify-content-center>' + rozvrhovaAkce.budova + '-' + rozvrhovaAkce.mistnost + '</ion-row><ion-row justify-content-center>' + rozvrhovaAkce.ucitel + '</ion-row></ion-col>';

            if (this.vyskaDnePole[cisloDneVTydnu] < rozvrhovaAkce.cisloRadku)
                this.vyskaDnePole[cisloDneVTydnu] = rozvrhovaAkce.cisloRadku;
        }

        this.vyskaDnePole[cisloDneVTydnu]++;
        this.vyskaDnePole[cisloDneVTydnu] *= this.vyskaRadku;
    }

}

/**
 * T��da reprezentuj�c� data rozvrhov� akce.
 * */
export class RozvrhovaAkce {
    roakIdno;
    katedra;
    predmet;
    nazev;
    ucitel;
    typAkceZkr;
    hodinaSkutOd;
    hodinaSkutOdHodinyMin;
    hodinaSkutDo;
    hodinaSkutDoHodinyMin;
    budova;
    mistnost;
    denZkr;
    tyden;
    platnost;
    cisloRadku = 0;
    barva = "#ffffff";

    constructor(json) {
        this.nastav(json);
    }

    nastav(json) {
        this.roakIdno = json.roakIdno ? json.roakIdno : "-";
        this.katedra = json.katedra ? json.katedra : "-";
        this.predmet = json.predmet ? json.predmet : "-";
        this.nazev = json.nazev ? json.nazev : "-";
        this.ucitel = json.vsichniUcitelePrijmeni ? json.vsichniUcitelePrijmeni : "-";
        this.typAkceZkr = json.typAkceZkr ? json.typAkceZkr : "-";
        this.hodinaSkutOd = json.hodinaSkutOd.value;
        this.hodinaSkutOdHodinyMin = this.hodinaSkutOd.split(":");
        this.hodinaSkutDo = json.hodinaSkutDo.value;
        this.hodinaSkutDoHodinyMin = this.hodinaSkutDo.split(":");
        this.budova = json.budova ? json.budova : "-";
        this.mistnost = json.mistnost ? json.mistnost : "-";
        this.denZkr = json.denZkr ? json.denZkr : "-";
        this.tyden = json.tyden ? json.tyden : "-";

        if (this.typAkceZkr == "Cv") {
            this.barva = "#ff8080";
        }
        else if (this.typAkceZkr == "Se") {
            this.barva = "#ff8566";
        }

        if (json.platnost == 'A') {
            this.platnost = "Plati";
        }
        else {
            this.platnost = "Neplati";
        }
    }

}
