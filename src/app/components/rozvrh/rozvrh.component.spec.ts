import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RozvrhComponent } from './rozvrh.component';

describe('RozvrhComponent', () => {
  let component: RozvrhComponent;
  let fixture: ComponentFixture<RozvrhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RozvrhComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RozvrhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
