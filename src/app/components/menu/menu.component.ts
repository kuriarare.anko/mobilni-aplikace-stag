import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Uzivatel } from '../../providers/uzivatel';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
    prihlasen;
    constructor(public actionSheetController: ActionSheetController, private router: Router, private uzivatel: Uzivatel) {
    }

    ngOnInit() {
        this.prihlasen = this.uzivatel.prihlasen;
    }

    /**
     * Vytvoří a zobrazí menu.
     * */
    async zobrazMenu() {
        const actionSheet = await this.actionSheetController.create({
            buttons: this.vytvorPolozkyVMenu()
        });
        await actionSheet.present();
    }

    /**
     * Vytvoří položky v menu.
     * */
    vytvorPolozkyVMenu() {
        let buttons = [];

        buttons.push({
            text: 'Vyhledávání',
            icon: 'search',
            cssClass: 'iconBarva',
            handler: () => {
                this.router.navigate(['vyhledat']);
            }
        });
        buttons.push({
            text: 'Moje známky',
            icon: 'clipboard',
            cssClass: 'iconBarva',
            handler: () => {
                this.router.navigate(['moje-znamky']);
            }
        });
        buttons.push({
            text: 'Zkoušky',
            icon: 'school',
            cssClass: 'iconBarva',
            handler: () => {
                this.router.navigate(['zkousky']);
            }
        });
        buttons.push({
            text: 'Plánovač',
            icon: 'list-box',
            cssClass: 'iconBarva',
            handler: () => {
                this.router.navigate(['planovac-zapsane']);
            }
        });
        buttons.push({
            text: 'Zobrazit',
            cssClass: 'podmenu',
            icon: 'remove',
            handler: () => {
                this.router.navigate(['planovac-zapsane']);
            }
        });
        buttons.push({
            text: 'Náplanovat / Upravit',
            cssClass: 'podmenu',
            icon: 'remove',
            handler: () => {
                this.router.navigate(['planovac']);
            }
        });
        buttons.push({
            text: 'Odhlásit',
            role: 'destructive',
            icon: 'log-out',
            cssClass: 'iconBarva',
            handler: () => {
                this.uzivatel.prihlasen = false;
                Cookie.delete('WSCOOKIE');
                this.router.navigate(['home']);
            }
        });
        buttons.push({
            text: 'Zavřít',
            icon: 'close',
            role: 'cancel',
            cssClass: 'iconBarva',
            handler: () => { }
        });
        return buttons;
    }

    /**
     * Přesměrovává na jinou stránku. - hlavni-strana, home
     * */
    domu() {
        if (this.uzivatel.prihlasen) {
            this.router.navigate(['hlavni-strana']);
        }
        else {
            this.router.navigate(['home']);
        }
    }

    /**
    * Přesměrovává na jinou stránku. - info-uzivatel
    * */
    info() {
        this.router.navigate(['info-uzivatel']);
    }

}
