import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { RozvrhComponent } from './rozvrh/rozvrh.component';


@NgModule({
    declarations: [MenuComponent, RozvrhComponent],
    exports: [MenuComponent, RozvrhComponent],
    imports: [CommonModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }