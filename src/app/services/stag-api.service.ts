import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Uzivatel } from '../providers/uzivatel';
import { Cookie } from 'ng2-cookies/ng2-cookies';

declare let window: any;

@Injectable({
    providedIn: 'root'
})
export class StagApiService {
    url = 'https://stag-ws.jcu.cz/ws/services/rest2/';
    semestr: string;

    constructor(private http: HTTP, private uzivatel: Uzivatel) {
        let datum = new Date();
        switch (datum.getMonth()) {
            case 0:
            case 8:
            case 9:
            case 10:
            case 11:
                {
                    this.semestr = 'ZS';
                    break;
                }
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                {
                    this.semestr = 'LS';
                    break;
                }
        }
    }

    /**
     * Otevře prohlížeč s webovou adresou pro přihlášení.
     * return Array(2) nakrajenaOdpoved['token'] - token přihlášeného, nakrajenaOdpoved['json'] - json pro nastavení uživatele        
     * */
    public prihlaseni(): Promise<any> {

        return new Promise(function (resolve, reject) {
            var prohlizec = window.cordova.InAppBrowser.open("https://stag-ws.jcu.cz/ws/login?originalURL=http://localhost/callback");
            prohlizec.addEventListener("loadstart", (event) => {
                if ((event.url).indexOf("http://localhost/callback") === 0) {
                    prohlizec.removeEventListener("exit", (event) => { });
                    prohlizec.close();
                    var odpoved = ((event.url).split("&"));

                    var nakrajenaOdpoved = {};
                    nakrajenaOdpoved['token'] = odpoved[0].split("=")[1];

                    var json = odpoved[3].split("=")[1];
                    Cookie.set('WSCOOKIE', nakrajenaOdpoved['token'], 1);
                    nakrajenaOdpoved['json'] = json;

                    resolve(nakrajenaOdpoved);
                }
            });
            prohlizec.addEventListener("exit", function (event) {
                reject("Přihlášení bylo přerušeno.");
            });
        });
    }


    async nastavUzivatele(nastavJson) {

        this.uzivatel.prihlasen = true;
        this.uzivatel.osCislo = nastavJson.stagUserInfo[0].userName;
        this.uzivatel.role = nastavJson.stagUserInfo[0].role;
        this.uzivatel.fakulta = nastavJson.stagUserInfo[0].fakulta;

        this.getStudentInfo(this.uzivatel.osCislo).then(value => {
            let jsonStudent = JSON.parse(value);

            this.uzivatel.jmeno = jsonStudent.jmeno;
            this.uzivatel.prijmeni = jsonStudent.prijmeni;
            this.uzivatel.fakultaSp = jsonStudent.fakultaSp;
            this.uzivatel.nazevSp = jsonStudent.nazevSp;
            this.uzivatel.kodSp = jsonStudent.kodSp;
            this.uzivatel.rocnik = jsonStudent.rocnik;
            this.uzivatel.email = jsonStudent.email;
            this.uzivatel.oborIdnos = jsonStudent.oborIdnos;

            this.uzivatel.tituly = jsonStudent.titulPred;
            if (jsonStudent.titulZa != null) {
                this.uzivatel.tituly += " " + jsonStudent.titulZa;
            }
            if (jsonStudent.tituly != null) {
                this.uzivatel.zobrazTituly = false;
            }
        });
    }
     
    //https://stag-ws.jcu.cz/ws/services/rest2/ucitel/najdiUcitelePodleJmena?jmeno=Anna
    async najdiUcitelePodleJmena(jmeno: string, prijmeni: string) {
        let params = { 'jmeno': "%", 'prijmeni': "%" };
        if (jmeno != undefined) {
            params.jmeno = jmeno + "%";
        }
        if (prijmeni != undefined) {
            params.prijmeni = prijmeni + "%";
        }

        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'ucitel/najdiUcitelePodleJmena', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/predmety/najdiPredmety?zkratka=UMB
    async najdiPredmety(pracoviste: string, zkratka: string, nazev: string) {
        let params = { 'nazev': "%", 'pracoviste': "%", 'zkratka': "%" };

        if (nazev != undefined) {
            params.nazev = nazev + "%";
        }
        if (pracoviste != undefined) {
            params.pracoviste = pracoviste.toUpperCase() + "%";
        }
        if (zkratka != undefined) {
            params.zkratka = zkratka.toUpperCase() + "%";
        }

        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'predmety/najdiPredmety', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/student/najdiStudentyPodleJmena?prijmeni=Kralova
    async najdiStudentyPodleJmena(jmeno: string, prijmeni: string) {
        let params = { 'jmeno': "%", 'prijmeni': "%", 'zkratka': "%" };

        if (jmeno != undefined) {
            params.jmeno = jmeno + "%";
        }
        if (prijmeni != undefined) {
            params.prijmeni = prijmeni + "%";
        }

        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'student/najdiStudentyPodleJmena', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/mistnost/getMistnostiInfo?zkrBudovy=BC
    async getMistnostiInfo(zkrBudovy: string, cisloMistnosti: string) {
        let params = { 'zkrBudovy': "%", 'cisloMistnosti': "%"};

        if (zkrBudovy != undefined) {
            params.zkrBudovy = zkrBudovy.toUpperCase() + "%";
        }
        if (cisloMistnosti != undefined) {
            params.cisloMistnosti = cisloMistnosti + "%";
        }

        const headers = { 'Accept': 'application/json'};

        const odpoved = await this.http.get(this.url + 'mistnost/getMistnostiInfo', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/student/getStudentInfo?osCislo=B16224
    async getStudentInfo(osCislo: string) {
        let params = { 'osCislo': osCislo };
        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'student/getStudentInfo', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/predmety/getPredmetInfo?katedra=UMB&zkratka=891
    async getPredmetInfo(katedra: string, zkratka: string) {

        let params = { 'katedra': katedra, 'zkratka': zkratka };
        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'predmety/getPredmetInfo', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/ucitel/getUcitelInfo?ucitIdno=7852
    async getUcitelInfo(ucitIdno: string) {

        let params = { 'ucitIdno': ucitIdno.toString() };
        const headers = { 'Accept': 'application/json' };
        const odpoved = await this.http.get(this.url + 'ucitel/getUcitelInfo', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/predmety/getPredmetyByUcitel?ucitIdno=7852
    async getPredmetyByUcitel(ucitIdno: string) {
        let params = { 'ucitIdno': ucitIdno };
        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'predmety/getPredmetyByUcitel', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/predmety/getPredmetyByStudent?osCislo=B16224&semestr=ZS login problem
    async getPredmetyByStudent(osCislo: string) {

        console.log(Cookie.get('WSCOOKIE')); //////////login problem
        let params = { 'osCislo': osCislo, 'semestr': this.semestr };
        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE')};

        const odpoved = await this.http.get(this.url + 'predmety/getPredmetyByStudent', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/student/getStudentPredmetyAbsolvoval?osCislo=B16224
    async getUzivatelPredmetyAbsolvoval() {
        let params = { 'stagUser': this.uzivatel.osCislo };

        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'student/getStudentPredmetyAbsolvoval', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/terminy/getTerminyProStudenta?osCislo=B16224
    async getTerminyProUzivatele() {
        let params = { 'osCislo': this.uzivatel.osCislo };

        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'terminy/getTerminyProStudenta', params, headers);
        return odpoved.data;
    }

    async zapisUzivateleNaTermin(termIdno:string) {
        let params = { 'osCislo': this.uzivatel.osCislo, 'termIdno': termIdno.toString() };

        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'terminy/zapisStudentaNaTermin', params, headers);
        return odpoved.data;
    }

    async odhlasUzivateleZTerminu(termIdno: string) {
        let params = { 'osCislo': this.uzivatel.osCislo, 'termIdno': termIdno.toString()};

        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'terminy/odhlasStudentaZTerminu', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/rozvrhy/getRozvrhByStudent?osCislo=B16210&&semestr=LS&&rok=2018  
    async getRozvrhByStudent(osCislo: string) {
        let params = { 'osCislo': osCislo, 'semestr': this.semestr}; //this.semestr
        console.log(osCislo);

        const headers = { 'Accept': 'application/json', 'Cookie': 'WSCOOKIE=' + Cookie.get('WSCOOKIE') };

        const odpoved = await this.http.get(this.url + 'rozvrhy/getRozvrhByStudent', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/rozvrhy/getRozvrhByUcitel?ucitIdno=3902&&semestr=LS
    async getRozvrhByUcitel(ucitIdno: string) {
        let params = { 'ucitIdno': ucitIdno, 'semestr': this.semestr};

        const headers = { 'Accept': 'application/json'};

        const odpoved = await this.http.get(this.url + 'rozvrhy/getRozvrhByUcitel', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/rozvrhy/getRozvrhByPredmet?katedra=KTS&&zkratka=VTVLS&semestr=LS
    async getRozvrhByPredmet(katedra: string, zkratka: string) {
        katedra = katedra.toString();
        zkratka = zkratka.toString();

        console.log(katedra);
        let params = { 'katedra': katedra, 'zkratka': zkratka, 'semestr': this.semestr};

        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'rozvrhy/getRozvrhByPredmet', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/rozvrhy/getRozvrhByMistnost?budova=PT&&mistnost=H2
    async getRozvrhByMistnost(budova: string, mistnost: string) {
        let params = { 'budova': budova, 'mistnost': mistnost };

        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'rozvrhy/getRozvrhByMistnost', params, headers);
        return odpoved.data;
    }

    //https://stag-ws.jcu.cz/ws/services/rest2/predmety/getPredmetyByOborFullInfo?oborIdno=1341
    async getPredmetyByOborFullInfo(oborIdno: string) {
        let params = { 'oborIdno': oborIdno};

        const headers = { 'Accept': 'application/json' };

        const odpoved = await this.http.get(this.url + 'predmety/getPredmetyByOborFullInfo', params, headers);
        return odpoved.data;
    }
}