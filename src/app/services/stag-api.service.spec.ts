import { TestBed } from '@angular/core/testing';

import { StagApiService } from './stag-api.service';

describe('StagApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StagApiService = TestBed.get(StagApiService);
    expect(service).toBeTruthy();
  });
});
