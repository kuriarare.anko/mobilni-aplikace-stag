import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class DbApiService {
    url = "https://tester-e30c.restdb.io/rest/studentiplanovac";

    constructor(private http: HTTP) {
        this.http.setDataSerializer('json');
    }

    /**
     * Metoda pro ulo�en� dat do datab�ze. Nejd��ve pomoc� metody nacti zjist� zda u�ivatel ji� n�co ulo�il
     * a na z�klad� tohoto zji�t�n� bu� vlo�� do datab�ze nov� z�znam �i jej p�ep�e.
     * @param data - data ukl�dan� do datab�ze
     * @param osCislo - osobn� ��slo studenta
     */
    async uloz(data, osCislo) {
        const headers = {
            'cache-control': 'no-cache',
            'x-apikey': '5e6be7f309c313436a6a0395',
            'Content-Type': 'application/json'
        };

        this.nacti(osCislo).then(value => {
            if (value != undefined) {
                this.http.put(this.url + "/" + value._id, data, headers);
            } else {
                this.http.post(this.url, data, headers);
            }
        });
    }

    /**
     * Metoda pro na�ten� dat z datab�ze.
     * @param osCislo - osobn� ��slo studenta
     */
    async nacti(osCislo) {
        const headers = {
            'cache-control': 'no-cache',
            'x-apikey': '5e6be7f309c313436a6a0395',
            'Content-Type': 'application/json'
        };
        let params = { 'q': '{"osCislo": "' + osCislo + '"}' };

        const odpoved = await this.http.get(this.url, params, headers);
        console.log(odpoved);
        let json = JSON.parse(odpoved.data);
        return json[0];
    }
}
