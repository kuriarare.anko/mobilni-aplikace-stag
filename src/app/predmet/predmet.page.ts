import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StagApiService } from 'src/app/services/stag-api.service';

@Component({
    selector: 'app-predmet',
    templateUrl: './predmet.page.html',
    styleUrls: ['./predmet.page.scss'],
})
export class PredmetPage implements OnInit {

    predmet: PredmetInfo;

    constructor(private route: ActivatedRoute, private router: Router, private stagApiService: StagApiService) {
    }

    /**
    * Pomocí parametrů předaných routerem nastaví proměnnou predmet.
    * */
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.predmet = new PredmetInfo(this.router.getCurrentNavigation().extras.state.json);
            }
        });
    }
}

/**
 * Třída reprezentující data předmětu.
 * */
export class PredmetInfo {
    katedra;
    zkratka;
    rok;
    nazev;
    nazevDlouhy;
    vyukaZS;
    vyukaLS;
    vyuka;
    kreditu;
    viceZapis;
    garanti;
    prednasejici;
    cvicici;
    podminujiciPredmety;
    vylucujiciPredmety;
    podminujePredmety;
    literatura;
    nahrazPredmety;
    metodyVyucovaci;
    metodyHodnotici;
    akreditovan;
    anotace;
    typZkousky;
    maZapocetPredZk;
    formaZkousky;
    prehledLatky;
    predpoklady;
    ziskaneZpusobilosti;
    vyucovaciJazyky;
    praxePocetDnu;
    automatickyUznavatZppZk;
    studijniOpory;
    rozsahPrednaska;
    rozsahCviceni;
    rozsahSeminare;
    zobrazRozsahPrednaska = true;
    zobrazRozsahCviceni = true;
    zobrazRozsahSeminare = true;
    id;


    constructor(json) {
        this.nastav(json);
    }

    nastav(infoJson) {

        let json = JSON.parse(infoJson);

        this.katedra = json.katedra;
        this.zkratka = json.zkratka;
        this.rok = json.rok ? json.rok : "-";;
        this.nazev = json.nazev ? json.nazev : "-";
        this.nazevDlouhy = json.nazevDlouhy ? json.nazevDlouhy : "-";

        console.log(json.vyukaZS);

        if (json.vyukaZS == 'A') {
            this.vyuka = "Zimní semestr";
            if (json.vyukaLS == 'A')
                this.vyuka += ", Letní semestr";
        }
        else if (json.vyukaLS == 'A') {
            this.vyuka = "Letní semestr";
            console.log(this.vyuka);
        }
        this.kreditu = json.kreditu ? json.kreditu : "-";
        this.viceZapis = json.viceZapis ? json.viceZapis : "-";
        this.garanti = json.garanti ? json.garanti : "-";
        this.prednasejici = json.prednasejici ? json.prednasejici : "-";
        this.cvicici = json.cvicici ? json.cvicici : "-";
        this.podminujiciPredmety = json.podminujiciPredmety ? json.podminujiciPredmety : "Nejsou definovány";
        this.vylucujiciPredmety = json.vylucujiciPredmety ? json.vylucujiciPredmety : "Nejsou definovány";
        this.podminujePredmety = json.podminujePredmety ? json.podminujePredmety : "Nejsou definovány";
        this.literatura = json.literatura ? json.literatura : "-";
        this.nahrazPredmety = json.nahrazPredmety ? json.nahrazPredmety : "Žádný";
        this.metodyVyucovaci = json.metodyVyucovaci ? json.metodyVyucovaci : "-";
        this.metodyHodnotici = json.metodyHodnotici ? json.metodyHodnotici : "-";

        this.akreditovan = json.akreditovan ? json.akreditovan : "-";
        if (this.akreditovan == 'A')
            this.akreditovan = "Ano";
        else if (this.akreditovan == 'N')
            this.akreditovan = "Ne";

        this.anotace = json.anotace ? json.anotace : "-";
        this.typZkousky = json.typZkousky ? json.typZkousky : "-";
        this.maZapocetPredZk = json.maZapocetPredZk ? json.maZapocetPredZk : "-";
        this.formaZkousky = json.formaZkousky ? json.formaZkousky : "-";
        this.prehledLatky = json.prehledLatky ? json.prehledLatky : "-";
        this.predpoklady = json.predpoklady ? json.predpoklady : "-";
        this.ziskaneZpusobilosti = json.ziskaneZpusobilosti ? json.ziskaneZpusobilosti : "-";
        this.vyucovaciJazyky = json.vyucovaciJazyky ? json.vyucovaciJazyky : "-";
        this.praxePocetDnu = json.praxePocetDnu ? json.praxePocetDnu : "-";

        this.automatickyUznavatZppZk = json.automatickyUznavatZppZk ? json.automatickyUznavatZppZk : "-";
        if (this.automatickyUznavatZppZk == 'A')
            this.automatickyUznavatZppZk = "Ano";
        else if (this.automatickyUznavatZppZk == 'N')
            this.automatickyUznavatZppZk = "Ne";

        this.studijniOpory = json.studijniOpory ? json.studijniOpory : "-";
        this.id = this.katedra + "/" + this.zkratka;

        if (json.jednotekPrednasek != '0') {
            this.rozsahPrednaska = "Prednaska " + json.jednotekPrednasek + " [" + json.jednotkaPrednasky + "]";
            this.zobrazRozsahPrednaska = false;
        }
        if (json.jednotekCviceni != '0') {
            this.rozsahCviceni = "Cviceni " + json.jednotekCviceni + " [" + json.jednotkaCviceni + "]";
            this.zobrazRozsahCviceni = false;
        }
        if (json.jednotekSeminare != '0') {
            this.rozsahSeminare = "Seminar " + json.jednotekSeminare + " [" + json.jednotkaSeminare + "]";
            this.zobrazRozsahSeminare = false;
        }
    }
}

