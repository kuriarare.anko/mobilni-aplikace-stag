import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredmetPage } from './predmet.page';

describe('PredmetPage', () => {
  let component: PredmetPage;
  let fixture: ComponentFixture<PredmetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredmetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredmetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
