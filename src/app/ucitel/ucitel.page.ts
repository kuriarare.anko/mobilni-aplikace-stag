import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StagApiService } from 'src/app/services/stag-api.service';


@Component({
    selector: 'app-ucitel',
    templateUrl: './ucitel.page.html',
    styleUrls: ['./ucitel.page.scss'],
})
export class UcitelPage implements OnInit {
    ucitel: UcitelInfo;
    zchovatInfo = false;
    zchovatPredmety = true;
    predmetyUcitel;

    constructor(private route: ActivatedRoute, private router: Router, private stagApiService: StagApiService) { }

    /**
    * Pomoc� parametr� p�edan�ch routerem nastav� prom�nnou ucitel a vyhled� p�edm�ty pro zobrazovan�ho u�itele.
    * */
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.ucitel = new UcitelInfo(this.router.getCurrentNavigation().extras.state.json);
            }
        });
        this.stagApiService.getPredmetyByUcitel(this.ucitel.ucitIdno).then(value => {
            this.nastavPredmety(value);
        });


    }

    /**
    * P�ep�na zobrazovan� informace.
    * @param id - 1 - informace o u�iteli, 2 - p�edm�ty u�itele
    */
    prepniZobrazeni(id) {
        switch (id) {
            case 1:
                this.zchovatInfo = false;
                this.zchovatPredmety = true;
                break;
            case 2:
                this.zchovatInfo = true;
                this.zchovatPredmety = false;
                break;
            default:
                break;
        }
    }

    nastavPredmety(jsonPredmet) {
        this.predmetyUcitel = new Array();
        let json = JSON.parse(jsonPredmet);
        let i = 0;
        for (const predmet of json.predmetUcitele) {
            this.predmetyUcitel[i] = new PredmetUcitel({ zkratka: predmet.katedra + "/" + predmet.zkratka, nazev: predmet.nazev });
            i++;
        }
    }

}

/**
 * T��da reprezentuj�c� data p�edm�tu.
 * */
export class PredmetUcitel {
    zkratka;
    nazev;
    constructor(data: PredmetUcitel) {
        Object.assign(this, data);
    }
}

/**
 * T��da reprezentuj�c� data u�itele.
 * */
export class UcitelInfo {
    ucitIdno;
    jmeno;
    prijmeni;
    tituly;
    zobrazTituly;
    platnost;
    katedra;
    email;
    telefon;
    url;

    constructor(jsonUcitel) {
        this.nastav(jsonUcitel);
    }

    nastav(infoJson) {
        let json = JSON.parse(infoJson);

        this.ucitIdno = json.ucitIdno + "";
        this.jmeno = json.jmeno ? json.jmeno : "-";
        this.prijmeni = json.prijmeni ? json.prijmeni : "-";

        this.tituly = json.titulPred;
        if (json.titulZa != null) {
            this.tituly += " " + json.titulZa;
        }
        if (json.tituly != null) {
            this.zobrazTituly = false;
        }

        this.platnost = json.platnost ? json.platnost : "-";
        this.katedra = json.katedra ? json.katedra : "-";
        this.email = json.email ? json.email : "-";
        this.telefon = json.telefon ? json.telefon : "-";
        this.url = json.url ? json.url : "-";

    }
}
