import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UcitelPage } from './ucitel.page';

describe('UcitelPage', () => {
  let component: UcitelPage;
  let fixture: ComponentFixture<UcitelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UcitelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UcitelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
