import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HlavniStranaPage } from './hlavni-strana.page';

describe('HlavniStranaPage', () => {
  let component: HlavniStranaPage;
  let fixture: ComponentFixture<HlavniStranaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HlavniStranaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HlavniStranaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
