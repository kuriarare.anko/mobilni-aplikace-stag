import { Component, OnInit } from '@angular/core';
import { Uzivatel } from '../providers/uzivatel';
import { StagApiService } from 'src/app/services/stag-api.service';

@Component({
    selector: 'app-hlavni-strana',
    templateUrl: './hlavni-strana.page.html',
    styleUrls: ['./hlavni-strana.page.scss'],
})
export class HlavniStranaPage implements OnInit {
    uzivCislo;
    predmety;
    constructor(private stagApiService: StagApiService, public uzivatel: Uzivatel) {
    }

    /**
     * Metoda z�sk� informace ze serveru IS/STAG a pot� je pou�ije pro nastaven� p�edm�t�.
     * */
    ngOnInit() {
        this.stagApiService.getPredmetyByStudent(this.uzivatel.osCislo).then(value => {
            this.nastavPredmety(value);
        });
    }

    nastavPredmety(jsonPredmety) {
        this.predmety = new Array();
        let json = JSON.parse(jsonPredmety);
        let i = 0;
        for (const item of json.predmetStudenta) {
            this.predmety[i] = new Predmet({
                katedra: item.katedra,
                zkratka: item.zkratka,
                nazev: item.nazev,
                kredity: item.kredity,
                statut: item.statut
            });
            i++;
        }
    }

}


/**
 * T��da reprezentuj�c� data p�edm�tu.
 * */
export class Predmet {
    katedra;
    zkratka;
    nazev;
    kredity;
    statut;

    constructor(init?: Partial<Predmet>) {
        Object.assign(this, init);
    }
}