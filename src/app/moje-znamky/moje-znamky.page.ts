import { Component, OnInit } from '@angular/core';
import { Uzivatel } from '../providers/uzivatel';
import { StagApiService } from 'src/app/services/stag-api.service';

@Component({
  selector: 'app-moje-znamky',
  templateUrl: './moje-znamky.page.html',
  styleUrls: ['./moje-znamky.page.scss'],
})
export class MojeZnamkyPage implements OnInit {

    predmety;

    constructor(public uzivatel: Uzivatel, private stagApiService: StagApiService) { }

    /**
     * Pomocí metody ze stagApiService nastaví proměnnou predmety.
     * */
    ngOnInit() {
        this.stagApiService.getUzivatelPredmetyAbsolvoval().then(value => {
            this.predmety = new Array();
            let json = JSON.parse(value);
            let i = 0;
            for (const predmet of json.predmetAbsolvoval) {
                this.predmety[i] = new Predmet({
                    katedra: predmet.katedra,
                    zkratka: predmet.zkratka,
                    rok: predmet.rok,
                    nazevPredmetu: predmet.nazevPredmetu,
                    semestr: predmet.semestr,
                    znamka: predmet.znamka,
                    pocetKreditu: predmet.pocetKreditu
                });
                i++;
            }
        }); 
  }
}

/**
 * Třída reprezentující data předmětu.
 * */
export class Predmet {
    katedra;
    zkratka;
    rok;
    nazevPredmetu;
    semestr;
    znamka;
    pocetKreditu;

    constructor(init?: Partial<Predmet>) {
        Object.assign(this, init);
        if (this.znamka == 'S')
            this.znamka = "Splněno";
        else if (this.znamka == 'N') {
            this.znamka = "Nesplněno";
        }
    }

}
