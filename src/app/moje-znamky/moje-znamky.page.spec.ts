import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MojeZnamkyPage } from './moje-znamky.page';

describe('MojeZnamkyPage', () => {
  let component: MojeZnamkyPage;
  let fixture: ComponentFixture<MojeZnamkyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MojeZnamkyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MojeZnamkyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
