import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanovacPage } from './planovac.page';

describe('PlanovacPage', () => {
  let component: PlanovacPage;
  let fixture: ComponentFixture<PlanovacPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanovacPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanovacPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
