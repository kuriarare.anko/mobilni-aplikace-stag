import { Component, OnInit } from '@angular/core';
import { Uzivatel } from '../providers/uzivatel';
import { StagApiService } from 'src/app/services/stag-api.service';
import { DbApiService } from 'src/app/services/db-api.service';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { PridatPredmetPage } from '../pridat-predmet/pridat-predmet.page';

@Component({
    selector: 'app-planovac',
    templateUrl: './planovac.page.html',
    styleUrls: ['./planovac.page.scss'],
})
export class PlanovacPage implements OnInit {
    predmety;
    planovanySemestr = "ZS";
    planovanySemestrCislo = 1; 
    zobraz;
    titulek = Math.ceil(this.planovanySemestrCislo/2) + ". ročník " + this.planovanySemestr; 
    vybrane;
    upozorneniPodminujici = "Podmiňující předmět není zapsán.";
    upozorneniVylucujici = "Je zapsán vylučující předmět."

    constructor(public uzivatel: Uzivatel, private stagApiService: StagApiService, private dbApiService: DbApiService, public alertController: AlertController, public modalController: ModalController, public toastController: ToastController) { }

    /**
     * Pomocí metod nastaví zobrazované předměty.
     * */
    ngOnInit() {
        this.stagApiService.getPredmetyByOborFullInfo(this.uzivatel.oborIdnos).then(value => {
            this.nastavPredmety(value);

            this.vybrane = new Map();
            this.vybrane.set(this.planovanySemestrCislo, new Array());
            this.dbApiService.nacti(this.uzivatel.osCislo).then(value => {
                if (value != undefined) {
                    this.vybrane = this.zJsonu(value.data);
                    this.predvypln();
                }
                console.log(this.vybrane);
            }); 
        });
    }

    nastavPredmety(jsonPredmety) {
        this.predmety = new Array();
        let json = JSON.parse(jsonPredmety);
        let i = 0;
        for (const predmet of json.predmetOboruFullInfo) {
            this.predmety[i] = new Predmet(predmet);
            i++;
        }
        this.zobraz = this.predmety.filter(function (item) {
            return item.vyukaZS == "A";
        });
    }

    klikDalsi() {
        this.planovanySemestrCislo++;
        this.nacteniPredmetu();
        
    }

    klikPredchozi() {
        this.planovanySemestrCislo--;
        if (this.planovanySemestrCislo < 1) {
            this.planovanySemestrCislo = 1;
            return;
        }
        this.nacteniPredmetu();
    }

    /**
     * Do proměnné zobraz na základě plánovaného semestru a informace zda je předmět již přidán do proměnné vybrane. Nastaví také titulek stránky.
     * */
    nacteniPredmetu() {
        if (!this.vybrane.has(this.planovanySemestrCislo)) {
            this.vybrane.set(this.planovanySemestrCislo, new Array());
        }
        this.zobraz = new Array();
        let pomoc = this.vybrane.get(this.planovanySemestrCislo);

        if (this.planovanySemestr == "ZS") {
            this.planovanySemestr = "LS";

            this.zobraz = this.predmety.filter(function (predmetKZobrazeni) {  
                return predmetKZobrazeni.vyukaLS == "A" && (!predmetKZobrazeni.jePridan || pomoc.filter(predmet => predmet.katedra == predmet.katedra && predmetKZobrazeni.zkratka == predmet.zkratka).length > 0);
            });
        } else {
            this.planovanySemestr = "ZS";
            this.zobraz = this.predmety.filter(function (predmetKZobrazeni) {
                return predmetKZobrazeni.vyukaZS == "A" && (!predmetKZobrazeni.jePridan || pomoc.filter(predmet => predmetKZobrazeni.katedra == predmet.katedra && predmetKZobrazeni.zkratka == predmet.zkratka).length > 0);
            });
        }
        this.titulek = Math.ceil(this.planovanySemestrCislo / 2) + ". ročník "  + this.planovanySemestr;
    }

    klikUlozit() {
        this.ulozDoNOSQL(this.vybrane);
        this.zobrazToast("Uloženo.");
    }

    /**
     * Přidá předmět do proměnné vybrane pokud není předmět vybrán, je-li již vybrán předmět naopak z proměnné odebere. Zavolá metodu pro zjištění 
     * zda lze předmět zapsat, pokud nesplňuje podmínky pro zapsání, zobrazí se upozornění.
     * @param e - volající události
     * @param predmet - předmět pro přidání
     */
    klikPridatPredmet(e, predmet) {
        let lzeZapsatPole = this.zjistiZdaLzeZapsat(predmet);
        if (!lzeZapsatPole[0] && !lzeZapsatPole[1]) {
            if (e.currentTarget.checked) {
                if (!(this.vybrane.get(this.planovanySemestrCislo).filter(item => item.katedra == predmet.katedra && item.zkratka == predmet.zkratka).length > 0)) {
                    predmet.jePridan = true;
                    this.vybrane.get(this.planovanySemestrCislo).push(predmet);
                }    
            }
            else if (this.vybrane.get(this.planovanySemestrCislo).includes(predmet)) {

                const index = this.vybrane.get(this.planovanySemestrCislo).indexOf(predmet);
                if (index > -1) {
                    this.vybrane.get(this.planovanySemestrCislo).splice(index, 1);
                }
                predmet.jePridan = false;
            }
        }
        else if (lzeZapsatPole[0]) {
            this.zobrazUpozorneni(this.upozorneniPodminujici);
            e.currentTarget.checked = false;
        }
        else if (lzeZapsatPole[1]) {
            this.zobrazUpozorneni(this.upozorneniVylucujici);
            e.currentTarget.checked = false;
        }
    }

    /**
     * Ověří zda předmět lze zapsat. Pokud nejsou v proměnné vybrne podmiňující předměty či jsou zapsané vylučující předměty
     * vrátí false a pokud je splněna podmínka vrátí true.
     * @param predmet - předmět, který se kontroluje
     * @return Array(2) - [0] - boolean pro kontrolu podmiňujících předmětů, [1] - boolean pro kontrolu vylučujících předmětů
     */
    zjistiZdaLzeZapsat(predmet) {
        let obsahujeVse = new Array;
        let vratit = new Array;

        console.log(predmet.podminujiciPredmety);
        if (predmet.podminujiciPredmety != undefined) {
            let podminujici = predmet.podminujiciPredmety.split(", ");

            for (let i = 0; i < podminujici.length; i++) {
                console.log(podminujici[i]);
                obsahujeVse[i] = this.zjistiZdaObsahuje(podminujici[i]);
            }
            if (obsahujeVse.includes(false)) {
                vratit[0] = true;
            } else {
                vratit[0] = false;
            }
        } else {
            vratit[0] = false;
        }

        obsahujeVse = new Array;


        if (predmet.vylucujiciPredmety != undefined) {
            let vylucujici = predmet.vylucujiciPredmety.split(", ");
            for (let i = 0; i < vylucujici.length; i++) {
                obsahujeVse[i] = this.zjistiZdaObsahuje(vylucujici[i]);
            }
            if (obsahujeVse.includes(true)) {
                vratit[1] = true;
            } else {
                vratit[1] = false;
            }
        } else {
            vratit[1] = false;
        }

        return vratit;
    }

    zjistiZdaObsahuje(predmet) {
        for (let entry of Array.from(this.vybrane.entries())) {
            for (let predmetVybrane of entry[1]) {
                if (predmetVybrane.katedra + "/" + predmetVybrane.zkratka == predmet) {
                    return true;
                }
            }
        }
        return false;
    }

    ulozDoNOSQL(data) {
        this.dbApiService.uloz(new ObjektProUlozeni({ osCislo: this.uzivatel.osCislo, data: JSON.stringify(Array.from(data.entries())) }), this.uzivatel.osCislo);
    }

    /**
     * Převede JSON předávaný v proměnné json do mapy. 
     * @param json - JSON pro převedení
     * @return Map - převedený JSON
     */
    zJsonu(json) {
        return new Map(JSON.parse(json));
    }

    async zobrazUpozorneni(text) {
        const upozorneni = await this.alertController.create({
            message: text,
            buttons: ['OK']
        });

        await upozorneni.present();
    }

    /**
     * Zavolá metodu zobrazPridatPredmet. Vrácený předmět projde kontrolou zda jej lze zapsat, pokud kontrolu splní
     * přidá se nebo se odebere, na základě toho zda byl předmět již přidán. Pokud kontrolu nesplní zobrazí se upozornění.
     * */
    klikJinyPredmet() {
        let existuje = null;
        let klicExistuje = null;
        this.zobrazPridatPredmet().then(value => {
            let json = JSON.parse(value.data);
            let predmet = new Predmet(json);

            let lzeZapsatPole = this.zjistiZdaLzeZapsat(predmet);
            if (!lzeZapsatPole[0] && !lzeZapsatPole[1]) {
                this.vybrane.forEach((hodnota: Array<Predmet>, klic: number) => {
                    for (let predmetPorvnavani of hodnota) {
                        if (predmet.katedra == predmetPorvnavani.katedra && predmet.zkratka == predmetPorvnavani.zkratka) {
                            existuje = predmetPorvnavani;
                            klicExistuje = klic;
                            break;
                        }
                    }
                });
                if (existuje == null) {
                    let predmetVZobraz = this.zobraz.find(hledany => hledany.katedra == predmet.katedra && hledany.zkratka == predmet.zkratka);
                    if (predmetVZobraz != null) {
                        predmetVZobraz.jePridan = true;
                    } else {
                        predmet.jePridan = true;
                        this.vybrane.get(this.planovanySemestrCislo).push(predmet);
                    }
                }
                else {
                    existuje.jePridan = false;
                    const index = this.vybrane.get(klicExistuje).indexOf(existuje);
                    if (index > -1) {
                        this.vybrane.get(klicExistuje).splice(index, 1);
                    }

                    let predmetVZobraz = this.zobraz.find(hledany => hledany.katedra == existuje.katedra && hledany.zkratka == existuje.zkratka);
                    if (predmetVZobraz != null) {
                        predmetVZobraz.jePridan = false;
                    }
                }
            }

            else if (lzeZapsatPole[0]) {
                this.zobrazUpozorneni(this.upozorneniPodminujici);
            }
            else if (lzeZapsatPole[1]) {
                this.zobrazUpozorneni(this.upozorneniVylucujici);
            }
        });
    }

    async zobrazPridatPredmet() {
        const modal = await this.modalController.create({
            component: PridatPredmetPage
        });

        const data = modal.onWillDismiss();
        await modal.present();
        return data;
    }

    /**
     * Zobrazí upozornění, které obsahuje seznam vybraných předmětů.
     * */
    klikZapsane() {
        let text = "Vybrané předměty <br><br>";
        for (let entry of this.vybrane.entries()) {
            text += entry[0] + " semestr<br>";
            for (let predmet of entry[1]) {
                text += predmet.nazev + "<br>";
            }
        }
        this.zobrazUpozorneni(text);
    }

    /**
     * Zaškrtne již vybrané předměty na zobrazovaný semestr.
     * */
    predvypln() {      
        for (let zobrazovanyPredmet of this.zobraz) {
            for (let zapsanyPredmet of this.vybrane.get(this.planovanySemestrCislo)) {
                if (zapsanyPredmet.katedra == zobrazovanyPredmet.katedra && zapsanyPredmet.zkratka == zobrazovanyPredmet.zkratka) {
                    zobrazovanyPredmet.jePridan = true;
                    break;
                }
            }
        }
    }

    async zobrazToast(zprava: string) {
        const toast = await this.toastController.create({
            message: zprava,
            duration: 2000
        });
        toast.present();
    }

}

/**
 * Třída pro uložení dat plánovače do databáze.
 * */
export class ObjektProUlozeni {
    osCislo;
    data;
    constructor(data: ObjektProUlozeni) {
        Object.assign(this, data);
    }
}

/**
 * Třída reprezentující data předmětu.
 * */
export class Predmet {
    katedra;
    zkratka;
    nazev;
    kreditu;
    statut;
    doporucenyRocnik;
    vyukaLS;
    vyukaZS;
    doporuceni = true;
    jePridan = false;
    podminujiciPredmety;
    vylucujiciPredmety;
    podminujePredmety;
    podminujiciPredmetyZobraz = true;
    vylucujiciPredmetyZobraz = true;
    podminujePredmetyZobraz = true;

    constructor(jsonPredmet) {
        this.katedra = jsonPredmet.katedra;
        this.zkratka = jsonPredmet.zkratka;

        this.nazev = jsonPredmet.nazev;
        this.kreditu = jsonPredmet.kreditu;

        this.statut = jsonPredmet.statut;

        if (jsonPredmet.doporucenyRocnik != null) {
            this.doporucenyRocnik = jsonPredmet.doporucenyRocnik + " " + (jsonPredmet.doporucenySemestr == null ? '' : jsonPredmet.doporucenySemestr);
            this.doporuceni = false;
        }
        this.vyukaLS = jsonPredmet.vyukaLS;
        this.vyukaZS = jsonPredmet.vyukaZS;

        if (jsonPredmet.podminujiciPredmety != null && jsonPredmet.podminujiciPredmety != '') {
            this.podminujiciPredmety = jsonPredmet.podminujiciPredmety;
            this.podminujiciPredmetyZobraz = false;
        }

        if (jsonPredmet.vylucujiciPredmety != null && jsonPredmet.vylucujiciPredmety != '') {
            this.vylucujiciPredmety = jsonPredmet.vylucujiciPredmety;
            this.vylucujiciPredmetyZobraz = false;
        }

        if (jsonPredmet.podminujePredmety != null && jsonPredmet.podminujePredmety != '') {
            this.podminujePredmety = jsonPredmet.podminujePredmety;
            this.podminujePredmetyZobraz = false;
        }

    }
}
