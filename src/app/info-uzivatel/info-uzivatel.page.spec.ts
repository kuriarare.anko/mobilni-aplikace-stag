import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoUzivatelPage } from './info-uzivatel.page';

describe('InfoUzivatelPage', () => {
  let component: InfoUzivatelPage;
  let fixture: ComponentFixture<InfoUzivatelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoUzivatelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoUzivatelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
