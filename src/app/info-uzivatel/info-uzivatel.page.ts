import { Component, OnInit } from '@angular/core';
import { Uzivatel } from '../providers/uzivatel';

@Component({
  selector: 'app-info-uzivatel',
  templateUrl: './info-uzivatel.page.html',
  styleUrls: ['./info-uzivatel.page.scss'],
})
export class InfoUzivatelPage implements OnInit {

    constructor(public uzivatel: Uzivatel) { }

  ngOnInit() { 
  }

}
