import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ZkouskyPage } from './zkousky.page';
import { ComponentsModule } from '../components/components.module'

const routes: Routes = [
  {
    path: '',
    component: ZkouskyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
        IonicModule,
        ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ZkouskyPage]
})
export class ZkouskyPageModule {}
