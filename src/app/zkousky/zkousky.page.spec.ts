import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZkouskyPage } from './zkousky.page';

describe('ZkouskyPage', () => {
  let component: ZkouskyPage;
  let fixture: ComponentFixture<ZkouskyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZkouskyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZkouskyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
