import { Component, OnInit } from '@angular/core';
import { StagApiService } from 'src/app/services/stag-api.service';
import { ToastController } from '@ionic/angular';

@Component({
    selector: 'app-zkousky',
    templateUrl: './zkousky.page.html',
    styleUrls: ['./zkousky.page.scss'],
})
export class ZkouskyPage implements OnInit {

    terminyMap;
    zobraz;
    upozorneni = true;

    constructor(private stagApiService: StagApiService, public toastController: ToastController) {
    }

    ngOnInit() {
        this.nactiTerminy();
    }

    /**
     * Pomoc� metody v stagApiService a zadan�ch parametr� vyhled� zkou�kov� term�ny, kter� n�sledn�
     * vlo�� do prom�nn� terminyMap. Tu pot� p�erovn� na z�klad� data uskute�n�n� a p�eulo�� do promenn� zobraz.
     * */
    nactiTerminy() {
        this.stagApiService.getTerminyProUzivatele().then(value => {
            console.log(value);
            let json = JSON.parse(value);
            if (json.termin.length > 0) {
                this.terminyMap = new Map();

                let i = 0;
                for (const termin of json.termin) {
                    let t = new Termin(termin);
                    if (!this.terminyMap.has(t.katedraPredmet)) {
                        this.terminyMap.set(t.katedraPredmet, new Array(t));
                    }
                    else {
                        this.terminyMap.get(t.katedraPredmet).push(t);
                    }
                }

                for (let item of this.terminyMap.values()) {
                    item = item.sort((s1, s2) => {
                        s1.datum.localeCompare(s2.datum) || s1.casOd.localeCompare(s2.casOd)
                    });
                }
                this.zobraz = Array.from(this.terminyMap.entries());
            }
            else {
                this.upozorneni = false;
            }
        });

    }

    getEntries() {
        this.zobraz = Array.from(this.terminyMap.entries());
    }

    /**
     * Ode�le pomoc� stagApiService dotaz na zaps�n� studenta na term�n a zobraz� toast s informac� o
     * poveden�/nepoveden� zaps�n�. Po �sp�n�m zaps�n� zavol� metodu pro na�ten� term�n�.
     * @param termIdno - identifika�n� ��slo zkou�kov�ho term�nu
     */
    klikZapsat(termIdno) {
        this.stagApiService.zapisUzivateleNaTermin(termIdno).then(value => {
            if (value === 'OK') {
                this.zobrazToast("Zaps�no.");
                this.nactiTerminy();
            }
            else {
                this.zobrazToast("Z�pis se nepovedl.")
            }
        });
    }

    /**
    * Ode�le pomoc� stagApiService dotaz na odeps�n� studenta na term�n a zobraz� toast s informac� o
    * poveden�/nepoveden� odeps�n�. Po �sp�n�m odeps�n� zavol� metodu pro na�ten� term�n�.
    * @param termIdno - identifika�n� ��slo zkou�kov�ho term�nu
    */
    klikOdepsat(termIdno) {
        this.stagApiService.odhlasUzivateleZTerminu(termIdno).then(value => {
            if (value === 'OK') {
                this.zobrazToast("Odeps�no.");
                this.nactiTerminy();
            }
            else {
                this.zobrazToast("Odeps�ni se nezda�ilo.")
            }
        });

    }

    async zobrazToast(zprava: string) {
        const toast = await this.toastController.create({
            message: zprava,
            duration: 2000
        });
        toast.present();
    }

}

/**
 * T��da reprezentuj�c� data zkou�kov�ho term�nu.
 * */
export class Termin {
    termIdno;
    katedraPredmet;
    datum;
    obsazeni;
    limit;
    casOd;
    casDo;
    budova;
    mistnost;
    opravny;
    deadlineDatumOdhlaseni;
    deadlineDatumPrihlaseni;
    schovatZapsat: boolean;
    schovatOdepsat: boolean;
    textDuvoduProcNelzeZapsatOdepsat;


    constructor(jsonTermin) {

        this.termIdno = jsonTermin.termIdno;
        this.katedraPredmet = jsonTermin.katedra + '/' + jsonTermin.predmet;

        this.datum = jsonTermin.datum.value ? jsonTermin.datum.value : "-";
        this.obsazeni = jsonTermin.obsazeni ? jsonTermin.obsazeni : "-";

        this.limit = jsonTermin.limit ? jsonTermin.limit : "-";
        this.casOd = jsonTermin.casOd ? jsonTermin.casOd : "-";
        this.casDo = jsonTermin.casDo ? jsonTermin.casDo : "-";
        this.budova = jsonTermin.budova ? jsonTermin.budova : "-";
        this.mistnost = jsonTermin.mistnost ? jsonTermin.mistnost : "-";
        if (jsonTermin.opravny == 'N') {
            this.opravny = "Ne";
        } ///co to vraci kdyz jo? A nebo cislo pokusu?
        else if (jsonTermin.opravny == 'A') {
            this.opravny = "Ano";
        }                  
        else {
            this.opravny = jsonTermin.opravny;
        }

        this.deadlineDatumOdhlaseni = jsonTermin.deadlineDatumOdhlaseni.value ? jsonTermin.deadlineDatumOdhlaseni.value : "-";
        this.deadlineDatumPrihlaseni = jsonTermin.deadlineDatumPrihlaseni.value ? jsonTermin.deadlineDatumPrihlaseni.value : "-";
        if (jsonTermin.zapsan == false && jsonTermin.lzeZapsatOdepsat == true) {
            this.schovatZapsat = false;
            this.schovatOdepsat = true;
        }
        else if (jsonTermin.zapsan == true && jsonTermin.lzeZapsatOdepsat == true) {
            this.schovatZapsat = true;
            this.schovatOdepsat = false;
        }
        else {
            this.schovatZapsat = true;
            this.schovatOdepsat = true;
        }
        this.textDuvoduProcNelzeZapsatOdepsat = jsonTermin.textDuvoduProcNelzeZapsatOdepsat ? jsonTermin.textDuvoduProcNelzeZapsatOdepsat : "";
    }

}