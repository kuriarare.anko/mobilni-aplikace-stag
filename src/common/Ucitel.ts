﻿class Ucitel {
    private ucitIdno: string;
    private jmeno: string;
    private prijmeni: string;
    private titulPred: string;
    private titulZa: string;

    constructor(ucitIdno: string, jmeno: string, prijmeni: string, titulPred: string, titulZa: string) {
        this.ucitIdno = ucitIdno;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.titulPred = titulPred;
        this.titulZa = titulZa;
    }

    vratUcitIdno() { return this.ucitIdno; }
    vratJmeno() { return this.jmeno; }
    vratPrijmeni() { return this.prijmeni; }
    vratTitulPred() { return this.titulPred; }
    vratTitulZa() { return this.titulZa; }
}